import {defineStore} from 'pinia'
import {drugCheckingDataStore} from './drug-checking-data';


export const appStatusStore = defineStore('appStatusStore', {
  state: () => ({
    showInfoDialog: false,
    showInfoSubstance: undefined
  }),
  actions: {
    openInfoDialog(substance) {
      this.showInfoSubstance = substance;
      this.showInfoDialog = true;
    },
    closeInfoDialog() {
      this.showInfoSubstance = undefined;
      this.showInfoDialog = false;
    },
  }
})
