import {defineStore} from 'pinia'
import {drugCheckingDataStore} from './drug-checking-data';

const drugCheckingData = drugCheckingDataStore();

export const userTestsStore = defineStore('testsStore', {
  state: () => ({
    tests: {},
    targetSubstance: undefined,
  }),
  getters: {
    testsArray(state) {
      return Object.keys(state.tests).map(reagentId => {
        var colorId = undefined;
        if (state.tests[reagentId] != null)
          colorId = state.tests[reagentId]['id'];
        if (state.tests[reagentId] === null)
          colorId = null;
        return {reagentId, colorId, colors: [colorId]}
      });
    },
    unusedReagents(state) {return drugCheckingData.reagentsList.filter(r => !(r.id in state.tests))},
    usedReagents(state) {return drugCheckingData.reagentsList.filter(r => r.id in state.tests)},
    suggestedReagents(state) {
      if (state.targetSubstance !== undefined) {
        var targetSubstance = state.targetSubstance;
        var substanceReactions = drugCheckingData.reactions.filter(reaction => reaction.substanceId == targetSubstance.id);
        var reagentSameReactions = {};
        substanceReactions.forEach(substanceReaction => {
          var sameReagentReactions = drugCheckingData.reactions.filter(reaction => reaction.reagentId == substanceReaction.reagentId && reaction.substanceId != targetSubstance.id);
          sameReagentReactions.forEach(async reagentReaction => {
            reagentReaction.colors.forEach(coloId => {
              if (substanceReaction.colors.includes(coloId)) {
                const sameCount = reagentSameReactions[substanceReaction.reagentId] ?? 0;
                reagentSameReactions[substanceReaction.reagentId] = sameCount + 1;
              }
            })
          });
        })
        const entries = Object.entries(reagentSameReactions);
        entries.sort((x, y) => x[1] - y[1]);
        reagentSameReactions = entries;

        var reagentSuggestion = [];
        for (var [i, r] of reagentSameReactions.entries()) {
          var reagent = state.unusedReagents.find(reagent => reagent['id'] == r[0]);
          if (reagent != null)
            reagentSuggestion.push(reagent);
          //if (reagentSuggestion.length > 4) break;
        }
        return reagentSuggestion;
      } else {
        return state.unusedReagents;
      }
    },
    computedResults(state) {
      var found = {};
      state.testsArray.forEach(test => {
        if (test['reagentId'] !== undefined && test['colorId'] !== undefined) {
          var reagentReactions = drugCheckingData.reactions.filter(r => r.reagentId == test['reagentId']);
          if (test['colorId'] != null) {
            var compatibleReactions = reagentReactions.filter(r => r.colors.includes(test['colorId']));
          } else {
            var compatibleReactions = reagentReactions.filter(r => r.isReacting == false);
          }
          compatibleReactions.forEach(substanceReaction => {
            const nfound = found[substanceReaction.substanceId] ?? 0;
            found[substanceReaction.substanceId] = nfound + 1;
          })
        }
      })
      // SORTING
      const entries = Object.entries(found);
      entries.sort((x, y) => x[1] - y[1]);
      found = entries;
      var max = Math.max(...found.map(o => o[1]))
      var result = [];
      for (var [i, r] of found.entries()) {
        if (r[1] == max) {
          var substance = drugCheckingData.substances.find(substance => substance['id'] == r[0]);
          result.push(substance);
        }
      }
      return result;
    }

  },
  actions: {
    reset() {
      this.tests = {};
      this.targetSubstance = undefined;
    },
    setReaction(reagent, color = undefined) {
      this.tests[reagent.id] = color;
    },
    removeReaction(reagent) {
      delete this.tests[reagent.id];
    },
    setTargetSubstance(substance) {
      this.targetSubstance = substance;
    }
  }
})
