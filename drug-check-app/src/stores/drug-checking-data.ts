import { defineStore } from 'pinia'
import reagents from '../data/reagents.json';
import colors from '../data/colors.json';
import reactions from '../data/results.json';
import substances from '../data/substances.json';

export const drugCheckingDataStore = defineStore('drugCheckingData', {
  state: () => ({
    reagents: reagents,
    colors: colors,
    reactions: reactions,
    substances: substances
  }),
  getters: {
    reagentsList: (state) => Object.values(state.reagents),
    colorsList: (state) => Object.values(state.colors),
    simpleColors: (state) => Object.values(state.colors).filter(c => c.simple == true),
  },
})
